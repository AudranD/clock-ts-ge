function hour2string(time: number): string {
  let result = Math.floor(time).toString();

  if (time < 10)
    return "0" + result;
  return result;
}

interface ClockUpdate {
  (hour: string, minutes: string, seconds: string): void;
}

class Clock {
  private static readonly SECONDS_IN_DAY = 86400;

  private time: number;
  private readonly callback: ClockUpdate;

  constructor(callback: ClockUpdate) {
    this.callback = callback;

    const date = new Date();
    this.time = date.getSeconds() + 60 * (date.getMinutes() + 60 * date.getHours());

    this.update();
    setInterval(() => this.addSeconds(1), 1000);
  }

  /**
  Compute time as string and run callback
   */
  private update() {
    let hours = hour2string(this.time / 3600);
    let minutes = hour2string((this.time / 60) % 60);
    let seconds = hour2string(this.time % 60);

    this.callback(hours, minutes, seconds);
  }

  /**
  Add `seconds` to the clock and update it
   */
  addSeconds(seconds: number) {
    this.time = (this.time + seconds) % Clock.SECONDS_IN_DAY;
    this.update();
  }

  /**
  Get time as string (format HH:MM:SS)
   */
  getTime(): string {
    let hours = hour2string(this.time / 3600);
    let minutes = hour2string((this.time / 60) % 60);
    let seconds = hour2string(this.time % 60);

    return `${hours}:${minutes}:${seconds}`;
  }
}

export {Clock, ClockUpdate};
