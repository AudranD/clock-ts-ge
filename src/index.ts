import './index.css';
import { Clock } from './clock';

enum ClockMode {
  Standard,
  IncreaseHours,
  IncreaseMinutes
}

class ClockApplication {
  private mode: ClockMode = 0;
  private clock: Clock;

  private readonly clockEl: Element;
  private readonly hoursEl: Element;
  private readonly minutesEl: Element;
  private readonly secondsEl: Element;

  constructor(clockEl: Element) {
    this.clockEl = clockEl;
    this.hoursEl = clockEl.getElementsByClassName("hours")[0];
    this.minutesEl = clockEl.getElementsByClassName("minutes")[0];
    this.secondsEl = clockEl.getElementsByClassName("seconds")[0];

    // add event listener to buttons
    clockEl.getElementsByClassName("mode-button")[0].addEventListener("click", this.changeMode.bind(this));
    clockEl.getElementsByClassName("increase-button")[0].addEventListener("click", this.increaseTime.bind(this));
    clockEl.getElementsByClassName("light-button")[0].addEventListener("click", this.lightClock.bind(this));

    // init clock
    this.clock = new Clock(this.callback.bind(this));
  }

  /**
   * Clock callback, updates HTML
   */
  callback(hours: string, minutes: string, seconds: string) {
    this.hoursEl.textContent = hours;
    this.minutesEl.textContent = minutes;
    this.secondsEl.textContent = seconds;
  }

  /**
   * Get HTML element corresponding to current mode (hours, minutes or null)
   */
  private getModeElement() {
    switch (this.mode) {
      case ClockMode.Standard:
        return null;
      case ClockMode.IncreaseHours:
        return this.hoursEl;
      default:
        return this.minutesEl;
    }
  }

  /**
   * Change mode by user request
   */
  changeMode() {
    let previousEl = this.getModeElement();
    if (previousEl != null) // stop blinking previous one
      previousEl.classList.remove("blink");

    // change mode value
    this.mode = (this.mode + 1) % 3;

    let newEl = this.getModeElement();
    if (newEl != null) // start blinking ne one
      newEl.classList.add("blink");
  }

  /**
   * Increase time by user request
   * Mode 1 (hours): 3600 seconds
   * Mode 2 (minutes): 60 seconds
   */
  increaseTime() {
    if (this.mode == ClockMode.IncreaseHours)
      this.clock.addSeconds(3600);
    else if (this.mode == ClockMode.IncreaseMinutes)
      this.clock.addSeconds(60);
  }

  /**
   * Enable light mode on clock for a few seconds
   */
  lightClock() {
    let el = this.clockEl.getElementsByClassName("time")[0];

    el.setAttribute("style", "color: red");

    setTimeout(function() {
      el.setAttribute("style", "color: black");
    }, 5000);
  }
}

new ClockApplication(document.getElementById("my-clock"));
